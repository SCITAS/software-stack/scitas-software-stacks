packages:
  all:
    target: ["{target}"]
    prefer: ['target={target}']

    require:
      - spec: '+ipo'
        when: '%gcc'

  # ----------------------------------------------------------------------------
  # Requirements
  # ----------------------------------------------------------------------------
  adios2:
    require: ['+kokkos', '+mgard', '~zfp']

  apr:
    # https://issues.apache.org/jira/browse/SVN-4813
    require: ['@1.6.2']

  arpack-ng:
    prefer: ['+mpi']

  boost:
    prefer: ['~mpi']
    require:
    - 'cxxstd=14'
    - '+icu'
    - '+atomic'
    - '+chrono'
    - '+container'
    - '+date_time'
    - '+filesystem'
    - '+graph'
    - '+iostreams'
    - '~json'
    - '+locale'
    - '+log'
    - '+math'
    - '~pic'
    - '+program_options'
    - '+random'
    - '+regex'
    - '+serialization'
    - '+shared'
    - '+signals'
    - '~singlethreaded'
    - '~stacktrace'
    - '+system'
    - '~taggedlayout'
    - '+test'
    - '+thread'
    - '+timer'
    - '~type_erasure'
    - '~versionedlayout'
    - '+wave'
    - '+exception'

    - spec: '+python +numpy'
      when: '%gcc'

    - spec: '+python +numpy'
      when: '%oneapi'

    - spec: '+python ~numpy'
      when: '%nvhpc'

  cairo:
    # ~X avoids a dependency to python (only prefer due to gtkplus)
    prefer: ['~X', '+svg']
    require: ['+fc', '+ft', '+pdf', '+gobject']

  cmake:
    prefer: ['~ncurses', '^curl tls=mbedtls']

  costa:
    require: ['~apps']

  cp2k:
    require: ['~ipo']

  curl:
    require:
    - '@8.7.1'
    - '+nghttp2'
    - '+libidn2'
    - '+libssh2'
    - one_of: ['tls=openssl', 'tls=mbedtls']
    prefer:
    - 'tls=mbedtls'

  dbcsr:
    require: ['smm=blas']

  emacs:
    prefer: ['+tls', '+X', 'toolkit=athena']

  expat:
    prefer: ['libbsd']

  fenics-dolfinx:
    require: ['@0.8.0', '+slepc']

  ffmpeg:
    require: ['+libx264']

  fftw:
    prefer: ['+mpi', '+openmp']

  gettext:
    require: ['+libxml2', '+xz']

  gnuplot:
    require: ['@5.4.10', '+cairo']

  hdf5:
    prefer:
    - spec: '@1.14.3'
    require:
    - spec: '+hl +threadsafe +szip +cxx +fortran'
    - spec: '+ipo'
      when: '%gcc'
    - spec: 'target={target}'
      when: '%gcc@{gcc_version}'


  hwloc:
    require: ["+libxml2", "+pci"]

  hypre:
    prefer:
    - spec: '@2.31.0 ~magma'

  intel-oneapi-mkl:
    prefer: ['~cluster']

  ior:
    require: ['+hdf5']

  julia:
    require: ['@1.10.4']

  kokkos:
    require: ['+numactl', '+memkind', '+serial',
              '+openmp', '~openmptarget',
              '~threads', '~sycl',
              '+aggressive_vectorization', '@4.3.01']

  kokkos-kernels:
    require: ['@4.3.01', '+openmp', '~threads', '~ipo']

  kokkos-nvcc-wrapper:
    prefer: ['@4.3.01']

  lammps:
    require: ['@20230802.3',
              'build_type=Release', '+asphere', '+atc', '+body', '+class2',
              '+colloid', '+compress', '+coreshell', '+dipole', '+diffraction',
              '+extra-dump', '+granular', '+h5md', '+kspace', '+latboltz',
              '+lib', '+manybody', '+mc', '+misc', '+molecule',
              '+mpi', '+netcdf', '+peri', '~poems', '+python', '+qeq',
              '+replica ', '+rigid', '+shock', '+ml-snap', '+srd', '+voronoi',
              '+plumed', '+kokkos']

  libfabric:
    require:
    - spec: 'fabrics=mlx,mrail,psm3,verbs,udp,tcp,sockets,shm'

  libffi:
    require: ['@3.4.6']

  libpressio:
    require: ['~qoz', '~sz3']

  libssh2:
    prefer: ['crypto=openssl']

  libtiff:
    require: ['build_system=cmake', '+pic', '+shared', '+jpeg']

  libtool:
    require: ['@2.4.6']

  libxc:
    require: ['build_system=cmake', '+shared']

  libxcb:
    require: ['+use_spack_interpreter']

  libxkbcommon:
    require: ['~wayland']

  libxml2:
    # python depends on libxml2 this avoid cyclic dependency
    require: ['~python']

  likwid:
    require: ['@5.3.0']

  llvm:
    # avoid compiler in lmod module
    prefer: ['~clang']
    require:
    - spec: '~cuda'
      when: '@15:'

  m4:
    require: ['+sigsegv']

  magma:
    # https://groups.google.com/a/icl.utk.edu/g/magma-user/c/l2XWTKmuBH0
    require: ['~ipo']

  mbedtls:
    prefer: ['@3.3.0', 'libs=shared,static']

  memkind:
    require: ['%gcc']

  mesa:
    require: ['~llvm', '+opengl', '+opengles', '+osmesa']

  meson:
    prefer: ['@1.2.1']

  metis:
    require: ['+real64']

  mgard:
    require: ['~cuda']

  mpfr:
    require: ['@4.0.2']

  mumps:
    require: ['+openmp', '+metis', '+parmetis']

  namd:
    require: ['interface=tcl']

  netcdf-c:
    prefer: ['+mpi']

  opari2:
    require: ['%gcc']

  openblas:
    require: ['build_system=makefile']
    prefer: ['threads=openmp', 'symbol_suffix=none', '~ilp64']

  opencv:
    prefer: ['+vtk', '+python3']

  openmpi:
    require: ['@5.0.3',
              'fabrics=ofi,ucc,ucx,verbs',
              'schedulers=slurm',
              '+romio', 'romio-filesystem=gpfs',
              '+internal-pmix', '~rsh',
              '~memchecker']

  openssl:
    prefer: ['+shared', 'certs=system', '~docs']

  otf2:
    require: ['%gcc']

  pango:
    prefer: ['~X']

  papi:
    require:
    - '+powercap'
    - '+rapl'
    - any_of: ['%gcc', '%oneapi']


  petsc:
    require: ['+hypre', '+mumps', '+saws', '+scalapack', '+mpi', '+hdf5',
              '+suite-sparse', '+kokkos', '+openmp', '+superlu-dist']

  python:
    require:
    - '@3.11.7'
    - '+ssl'
    - '+tkinter'
    - '+crypt'
    - '+bz2'
    - '+pyexpat'
    - '+libxml2'
    - '+ctypes'
    - '+uuid'
    - '+sqlite3'
    - spec: '+optimizations'
      when: '%gcc'
    prefer:
    - spec: '%gcc@{gcc_version}'
      when: '%gcc'

  py-cppy:
    require: ['@1.2.1']

  py-cython:
    require:
    - one_of: ['@0.29.36', '@3.0.8']
    prefer: ['@3.0.8']

  py-python-dateutil:
    require: ['@2.8.2']

  py-fenics-dolfinx:
    require: ['@0.8.0']

  py-horovod:
    require: ['frameworks=pytorch,keras,tensorflow', 'controllers=mpi', 'tensor_ops=nccl']

  py-kiwisolver:
    require: ['@1.4.5']

  py-matplotlib:
    require: ['@3.3.4']

  py-mpmath:
    require: ['@1.2.1']

  py-numpy:
    prefer: ['@1.26.4']

  py-pandas:
    prefer: ['@2.2.1']

  py-pillow:
    prefer: ['@9.5.0', '+jpeg']

  py-pysam:
    prefer: ['@0.22.1']

  # https://github.com/tensorflow/tensorflow/issues/63360#issuecomment-2142735122
  # MKL support seams not maintained
  py-tensorflow:
    require: ['+mpi', '~mkl']

  py-torch:
    require: ['@2.3.1', '+mpi', '~valgrind']

  py-scipy:
    require: ['@1.13.0']

  py-setuptools:
    require: ['@63.4.3']

  py-virtualenv:
    require: ['@20.24.5']

  qt:
    require: ['@5.15.2']

  quantum-espresso:
    require: ['+mpi', '+openmp', '~gipaw', '+scalapack', '+libxc', '~ipo']
    prefer: ['hdf5=parallel']

  rust:
    require: ['+dev']

  scotch:
    prefer: ['~mpi']
    require: ['build_system=cmake']

  sirius:
    prefer: ['+fortran', '~elpa', '+wannier90']
    require: ['+openmp']

  slepc:
    require:
    - spec: '^arpack-ng +mpi'

  spades:
    require: ['~ipo']

  suite-sparse:
    require:
      - '+graphblas'
      - one_of: ['@7.3.1', '@7.2.1']
    prefer: ['@7.3.1']

  superlu:
    require: ['@5.3.0']

  tk:
    require: ['+xft', '+xss']

  ucx:
    require: ['@1.17.0',
              '+rdmacm',
              '+rc', '+dc', '+ud',
              '+cma', '+dm',
              '+verbs', '+mlx5_dv', '+ib_hw_tm']

  vtk:
    require:
    - spec: '~python ++mpi +xdmf +ffmpeg'

  xmlto:
    require: ['@0.0.29']

  # ----------------------------------------------------------------------------
  # System dependencies
  # ----------------------------------------------------------------------------
  egl:
    buildable: false
    externals:
    - spec: 'egl@21.3.4'
      prefix: /usr

  ninja:
    buildable: false
    externals:
    - spec: 'ninja@1.10.2'
      prefix: /usr

  opengl:
    buildable: false
    externals:
    - spec: 'opengl@4.6'
      prefix: /usr

  pmix:
    buildable: false
    externals:
    - spec: 'pmix@5.0.1'
      prefix: /usr

  rdma-core:
    buildable: false
    externals:
    - spec: 'rdma-core@47.1'
      prefix: /usr

  slurm:
    buildable: false
    externals:
    - spec: 'slurm@23-11-7-1'
      prefix: /usr

  # ----------------------------------------------------------------------------
  # Externals
  # ----------------------------------------------------------------------------
  abaqus:
    buildable: false
    permissions: {read: group, group: abaqus-soft}
    externals:
    - {spec: abaqus@2019, prefix: /ssoft/spack/external/abaqus/2019}
    - {spec: abaqus@2023, prefix: /ssoft/spack/external/abaqus/2023}
  ansys:
    buildable: false
    externals:
    - {spec: ansys@2020R2, prefix: /ssoft/spack/external/ansys/2020R2/v202}
    - {spec: ansys@2022R1, prefix: /ssoft/spack/external/ansys/2022R1/v221}
    - {spec: ansys@2022R2, prefix: /ssoft/spack/external/ansys/2022R2/v222}
    - {spec: ansys@2024R1, prefix: /ssoft/spack/external/ansys/2024R1/v241}
    - {spec: ansys@2024R2, prefix: /ssoft/spack/external/ansys/2024R2/v242}
  cfdplusplus:
    buildable: false
    externals:
    - {spec: cfdplusplus@16.1, prefix: /ssoft/spack/external/CFD++/2016.05}
    - {spec: cfdplusplus@19.1, prefix: /ssoft/spack/external/CFD++/19.1}
  comsol:
    buildable: false
    permissions: {read: group, group: comsol-soft}
    externals:
    - {spec: comsol@5.6, prefix: /ssoft/spack/external/comsol/5.6/comsol56/multiphysics/}
    - {spec: comsol@6.0, prefix: /ssoft/spack/external/comsol/6.0}
    - {spec: comsol@6.2, prefix: /ssoft/spack/external/comsol/6.2}
  fdtd:
    buildable: false
    permissions: {read: group, group: ansys-soft}
    externals:
    - {spec: fdtd@2020-R2-2387, prefix: /ssoft/spack/external/fdtd/8.24.2387}
    - {spec: fdtd@2020-R2.4-2502, prefix: /ssoft/spack/external/fdtd/2020-R2.4-2502}
    - {spec: fdtd@2021-R2.2-2806, prefix: /ssoft/spack/external/fdtd/2021-R2.2-2806}
    - {spec: fdtd@2022-R1.1-2963, prefix: /ssoft/spack/external/fdtd/2022-R1.1-2963}
    - {spec: fdtd@2024-R2.3-3941, prefix: /ssoft/spack/external/fdtd/2024-R2.3-3941}
  gaussian:
    buildable: false
    externals:
    - {spec: gaussian@g16-A.03, prefix: /ssoft/spack/external/gaussian/g16-A.03/avx2}
    - {spec: gaussian@g16-C.01, prefix: /ssoft/spack/external/gaussian/g16-C.01/avx2}
  maple:
    buildable: false
    externals:
    - {spec: maple@2017, prefix: /ssoft/spack/external/Maple/2017}
  mathematica:
    buildable: false
    externals:
    - {spec: mathematica@11.1.1, prefix: /ssoft/spack/external/Mathematica/11.1.1}
    - {spec: mathematica@13.0, prefix: /ssoft/spack/external/Mathematica/13.0}
  matlab:
    buildable: false
    externals:
    - {spec: matlab@R2018a, prefix: /ssoft/spack/external/MATLAB/R2018a}
    - {spec: matlab@R2019b, prefix: /ssoft/spack/external/MATLAB/R2019b}
    - {spec: matlab@R2024a, prefix: /ssoft/spack/external/MATLAB/R2024a}
    - {spec: matlab@R2024b, prefix: /ssoft/spack/external/MATLAB/R2024b}
  molpro:
    buildable: false
    externals:
    - {spec: molpro@2022.3.0, prefix: /ssoft/spack/external/molpro/2022.3.0/mpi}
  smr:
    buildable: false
    externals:
    - {spec: smr@2017.0, prefix: /ssoft/spack/external/SMR/2017.06}
  totalview:
    buildable: false
    externals:
    - {spec: totalview@2017.2.11, prefix: /ssoft/spack/external/toolworks/totalview.2017.2.11}
    - {spec: totalview@2020.3.11, prefix: /ssoft/spack/external/toolworks/totalview.2020.3.11}
