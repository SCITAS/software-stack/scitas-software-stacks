#!/usr/bin/env sh

set -o errexit
set -o pipefail
set -o nounset

source ${CI_PROJECT_DIR}/ci/stack_env.sh

_compilers=$(jq -Mrc '.stack
  | .system_arch as $arch
  | .compilers
  | to_entries
  | .[]
  | {name: .key, arch: $arch} + .value
  | select((.constraint == null) or (.constraint? | index(env.environment)))
' ${STACK_CONFIG})

compilers_names="gcc $(echo ${_compilers} | jq -Mrs '. | map("\(.name)") | .[]' | egrep -v '^gcc$')"

for name in ${compilers_names}
do
    source ci/stack_env.sh ${name}

    # Cleaning patches in case one has a different hash
    ${STACK_LOCATION}/spack/bin/spack \
        --color always \
        -e ${full_environment} \
        clean -m

    ${STACK_LOCATION}/spack/bin/spack \
        --color always \
        -e ${full_environment} \
        install \
        --log-file ${CI_PROJECT_DIR}/spack-install-${full_environment}.xml \
        --log-format junit \
        --only-concrete \
        --fail-fast \
        --show-log-on-error

    ${STACK_LOCATION}/spack/bin/spack \
        --color always \
        -e ${full_environment} \
        find -vl | tee spack-find-${full_environment}-${stack}.log

    ${STACK_LOCATION}/spack/bin/spack \
        --color always \
        -e ${full_environment} \
        buildcache create \
        --update-index \
        --key ${GPG_KEY_ID} \
        ${MOUNT_POINT}/buildcache/$environment

    echo "Checking ${STACK_CONFIG_PATH}/mark_explicit_${full_environment} for spec to mark explicit"
    if [ -e ${STACK_CONFIG_PATH}/mark_explicit_${full_environment} ]
    then
        while read -r spec
        do
            echo "Mark ${spec} as explicit"
            ${STACK_LOCATION}/spack/bin/spack \
                --color always \
                -e ${full_environment} \
                mark \
                --explicit \
                ${spec}
        done <<< $(cat ${STACK_CONFIG_PATH}/mark_explicit_${full_environment} )
    fi

    ${STACK_LOCATION}/spack/bin/spack \
        --color always \
        -e ${full_environment} \
        module lmod refresh \
        --yes | tee spack-modules-${full_environment}-${stack}.log
done
