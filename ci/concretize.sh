#!/usr/bin/env sh

set -o errexit
set -o pipefail
set -o nounset

if [ $# -ge 1 ]
then
  force="$*"
  echo "Extra concretize arguments ${force}"
else
  force=""
fi

source ${CI_PROJECT_DIR}/ci/stack_env.sh

_compilers=$(jq -Mrc '.stack
  | .system_arch as $arch
  | .compilers
  | to_entries
  | .[]
  | {name: .key, arch: $arch} + .value
  | select((.constraint == null) or (.constraint? | index(env.environment)))
' ${STACK_CONFIG})

compilers_names="gcc $(echo ${_compilers} | jq -Mrs '. | map("\(.name)") | .[]' | egrep -v '^gcc$')"

for name in ${compilers_names}
do
  source ci/stack_env.sh ${name}

  echo "Checking /deconcretize/${stack}_${full_environment}_${SQUASHFS_ID}"
  if [ -e /deconcretize/${stack}_${full_environment}_${SQUASHFS_ID} ];
  then
     while read -r spec
     do
       echo "Deconcretizing ${spec}"
       ${STACK_LOCATION}/spack/bin/spack \
           --color=always \
           -e ${full_environment} \
           deconcretize \
           --all --yes-to-all \
           ${spec}
     done <<< $(cat /deconcretize/${stack}_${full_environment}_${SQUASHFS_ID})
  fi

  ${STACK_LOCATION}/spack/bin/spack \
      --color always \
      -e ${full_environment} \
      config blame | tee config-${full_environment}-${stack}.log

  ${STACK_LOCATION}/spack/bin/spack \
      --color always \
      -e ${full_environment} \
      concretize ${force} | tee concretize-${full_environment}-${stack}.log

  ${STACK_LOCATION}/spack/bin/spack \
      --color always \
      -e ${full_environment} \
      mirror create \
      -D -d ${MOUNT_POINT}/spack-mirror -a || /usr//bin/true

  cp ${SPACK_SYSTEM_CONFIG_PATH}/spack.lock spack-${full_environment}-${stack}.lock
done
