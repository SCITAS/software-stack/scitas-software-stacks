#!/usr/bin/env sh

set -o errexit
set -o pipefail
set -o nounset

for i in $(ls -1d ${CI_DATA_ST}/overlayfs/*-${stack}-${environment}-${SQUASHFS_ID}-*)
do
    id=$(echo $i | sed -e "s/.*-\([0-9]\+\)\(-[a-z0-9]\+\)\?/\1/")
    if [ $id -le ${CI_PIPELINE_ID} ]
    then
        echo "Cleaning $i"
        rm -rf $i
    fi
done
