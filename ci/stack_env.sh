#!/usr/bin/env sh

if [ $# -eq 1 ]
then
    name=$1
    suffix="-${name}"
    if [ "${name}" == "gcc" ]
    then
        suffix=""
    fi
else
    suffix=""
fi


export STACK_CONFIG_PATH=${CI_PROJECT_DIR}/stacks/${stack}
export STACK_CONFIG=${STACK_CONFIG_PATH}/config.json

export SPACK_VERSION=$(jq -r .spack.version ${STACK_CONFIG})
export MOUNT_POINT=$(jq -r .stack.mount_point ${STACK_CONFIG})
export STACK_VERSION=$(jq -r .stack.version ${STACK_CONFIG})
export STACK_LOCATION=${MOUNT_POINT}/${stack}/${environment}/${STACK_VERSION}


export full_environment=${environment}${suffix}
export SPACK_SYSTEM_CONFIG_PATH=${STACK_LOCATION}/spack/var/spack/environments/${full_environment}

#export SPACK_USER_CACHE_PATH=$(mktemp -p /tmp -d slurm_user_cache_XXXXXXX)
#export SPACK_USER_CONFIG_PATH=$(mktemp -p /tmp -d slurm_user_config_XXXXXXX)

export SPACK_VERSION=$(jq -r .spack.version ${STACK_CONFIG})

env_json=$(jq '.stack.environments | to_entries | .[] | select(.key == env.environment) | .value' ${STACK_CONFIG})

export environment_type=$(echo $env_json | jq -Mrc '.type // "local_cluster"')
export target=$(echo $env_json | jq -Mrc '.target')
export accelerator=$(echo $env_json | jq -Mrc '.accelerator // "none"')

if [ "$accelerator" != "none" ]
then
    export acc_type=$(echo $env_json | jq -Mrc '.accelerator.type')
    export acc_arch=$(echo $env_json | jq -Mrc '.accelerator.arch')
else
    export acc_type="none"
fi

set +u
if [ "x$GPG_KEY_ID" == "x" ]
then
    export GPG_KEY_ID=$(gpg --show-keys $GPG_PRIVATE_KEY 2> /dev/null | head -n 2 | tail -1)
fi
set -u

echo "STACK_CONFIG_PATH: ${STACK_CONFIG_PATH}"
echo "STACK_CONFIG: ${STACK_CONFIG}"
echo "SPACK_VERSION: ${SPACK_VERSION}"
echo "MOUNT_POINT: ${MOUNT_POINT}"
echo "STACK_LOCATION: ${STACK_LOCATION}"
echo "SPACK_SYSTEM_CONFIG_PATH: ${SPACK_SYSTEM_CONFIG_PATH}"

if [ "$accelerator" == "none" ]
then
    echo "{ \"target\": \"${target}\", \"type\": \"${environment_type}\", \"accelerator\": \"${accelerator}\" }" | jq -C .
else
    echo "{ \"target\": \"${target}\", \"type\": \"${environment_type}\", \"accelerator\": ${accelerator} }" | jq -C .
fi

# echo "SPACK_USER_CACHE_PATH: ${SPACK_USER_CACHE_PATH}"
# echo "SPACK_USER_CONFIG_PATH: ${SPACK_USER_CONFIG_PATH}"

set +o nounset

if [ "x${CI_DEFAULT_BRANCH}" != "x" ]
then
   squash_base=${CI_DEFAULT_BRANCH}
else
   squash_base=main
fi


set -o nounset
