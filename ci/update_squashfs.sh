#!/usr/bin/env sh

set -o nounset
set -o errexit
set -o pipefail

source ${CI_PROJECT_DIR}/ci/stack_env.sh

sqfs_suffix="${CI_PIPELINE_ID}-$(date +%Y%m%d-%H%M)"

if [ $# -eq 1 ]
then
  sqfs_suffix=$1
  echo "Got 1 argument"
fi

jq -Mr '
.spack.mirrors
  | to_entries
  | map(select(.value.type="relative"))
  | map("\(.value.url)")
  | .[]
' ${STACK_CONFIG} > excludes.list
echo buildcache >> excludes.list
echo external >> excludes.list

cd /squashfs-cache

mksquashfs ${MOUNT_POINT} ${stack}-${environment}-${SQUASHFS_ID}-${sqfs_suffix}.sqfs -ef ${CI_PROJECT_DIR}/excludes.list

cd -
