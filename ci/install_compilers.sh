#!/usr/bin/env sh

set -o errexit
set -o pipefail
set -o nounset

source ${CI_PROJECT_DIR}/ci/stack_env.sh

system_compiler=$(jq -Mrc '
.stack
 | .system_arch as $arch
 | .system_compiler
 | map("\(.compiler)@\(.version) \($arch)")
' ${STACK_CONFIG})

_compilers=$(jq -Mr '.stack
  | .system_arch as $arch
  | .compilers
  | to_entries
  | .[]
  | {name: .key, arch: $arch} + .value
  | select((.constraint == null) or (.constraint? | index(env.environment)))
' ${STACK_CONFIG})

# list compilers and ensure gcc is first
compilers_names="gcc $(echo ${_compilers} | jq -Mrs '. | map("\(.name)") | .[]' | egrep -v '^gcc$')"

echo ${compilers_names}

echo "List compilers:"
echo ${_compilers} | jq -C '.'

main_env_path=${STACK_LOCATION}/spack/var/spack/environments/${environment}

for name in ${compilers_names}
do
    spec=$(echo $_compilers  | jq --arg name $name -Mrc '. | select(.name==$name) | "\(.spec) \(.arch)"')

    source ${CI_PROJECT_DIR}/ci/stack_env.sh ${name}

    echo "definitions:" > ${SPACK_SYSTEM_CONFIG_PATH}/compiler_definitions.yaml
    compilers=$(echo $_compilers  | jq -Mrc --arg name "$name" '. | select(.name==$name) | ["\(.name)@\(.version) target=\(env.target)"]')
    compilers_specs="[$spec]"
    for c in "system_compiler" "compilers" "compilers_specs"; do
        if [ "x${!c}" != "x" ]; then
            echo " - ${c}: ${!c}" >> ${SPACK_SYSTEM_CONFIG_PATH}/compiler_definitions.yaml
        fi
    done
    echo " - compiler_${name}: ${compilers}" >> ${SPACK_SYSTEM_CONFIG_PATH}/compiler_definitions.yaml

    echo "Compilers for environmnent ${full_environment}"
    cat ${SPACK_SYSTEM_CONFIG_PATH}/compiler_definitions.yaml


    echo "Getting system compiler"
    ${STACK_LOCATION}/spack/bin/spack compiler find --scope system /usr

    echo "Installing compiler ${name}: ${spec}"
    ${STACK_LOCATION}/spack/bin/spack \
        --color always \
        spec \
        --install-status \
        --namespaces \
        --long \
        --types \
        ${spec}

    ${STACK_LOCATION}/spack/bin/spack \
        --color always \
        spec \
        --install-status \
        --namespaces \
        --long \
        --types \
        --json \
        ${spec} > compilers_spec.json

    ${STACK_LOCATION}/spack/bin/spack \
        --color always \
        install \
        --log-file ${CI_PROJECT_DIR}/spack-install-${name}.xml \
        --log-format junit \
        --fail-fast \
        --show-log-on-error \
        ${spec}

    hashes=$(jq -Mr '.spec.nodes | map("/\(.hash)") | join(" ")' compilers_spec.json)

    echo "Adding packages to buildcache"
    ${STACK_LOCATION}/spack/bin/spack \
        --color always \
        buildcache create \
        --update-index \
        --key ${GPG_KEY_ID} \
        ${MOUNT_POINT}/buildcache/${environment} ${hashes}

    for compiler_hash in $(echo ${hashes}); do
        location=$(${STACK_LOCATION}/spack/bin/spack location -i ${compiler_hash})

        ${STACK_LOCATION}/spack/bin/spack \
            --color always \
            compiler find \
            --scope system \
            ${location} \
            ${location}/compiler/latest \
            ${location}/compiler/latest/linux \
            ${location}/Linux_x86_64/2024/compilers
    done
done
