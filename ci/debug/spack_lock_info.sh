#!/usr/bin/env sh

get_all_dep(){
    file=$2
    deps=$(jq --arg h "$1" -Mrc '.concrete_specs | to_entries | map(select(.key | match($h))) | .[] | .value.dependencies[] | "\(.hash)"' $file)
    for i in $deps
    do
        jq --arg h "$i" -c '.concrete_specs
            | to_entries
            | map(select(.key | match($h)))
            | .[]
            | {
                name: .value.name,
                key: .key,
                version: .value.version,
                compiler: (.value.compiler | "\(.version)"),
                target: (.value.arch.target | if type == "object" then .name else . end),
                dep: .value.dependencies[]
                     | {
                         name: .name,
                         key: .hash,
                         type: .parameters.deptypes,
                         virtuals: .parameters.virtuals
                       }
              }' $file
    done
}

get_hash_dep(){
    jq --arg h "$1" -c '.concrete_specs
            | to_entries
            | map(select(.key | match($h)))
            | .[]
            | {
                name: .value.name,
                key: .key,
                version: .value.version,
                compiler: (.value.compiler | "\(.version)"),
                target: (.value.arch.target | if type == "object" then .name else . end),
                dep: .value.dependencies[]
                     | {
                         name: .name,
                         key: .hash,
                         type: .parameters.deptypes,
                         virtuals: .parameters.virtuals
                       }
              }' $2
}

get_name_dep(){
    jq --arg n "$1" -c '.concrete_specs
            | to_entries
            | map(select(.value.name == $n))
            | .[]
            | {
                name: .value.name,
                key: .key,
                version: .value.version,
                compiler: (.value.compiler | "\(.version)"),
                target: (.value.arch.target | if type == "object" then .name else . end),
                dep: .value.dependencies[]
                     | {
                         name: .name,
                         key: .hash,
                         type: .parameters.deptypes,
                         virtuals: .parameters.virtuals
                       }
              }' $2
}


usage() {
    echo "-h [hash]"
    echo "-n [name]"
    echo "-f [file]"
}

OPTSTRING=":h:n:f:a"

all=0
hash=""
name=""
file=""
while getopts ${OPTSTRING} opt; do
  case ${opt} in
    f)
        file=${OPTARG}
        ;;
    h)
        hash=${OPTARG}
        ;;
    n)
        name=${OPTARG}
        ;;
    a)
        all=1
        ;;
    :)
      echo "Option -${OPTARG} requires an argument."
      usage
      exit 1
      ;;
    ?)
      echo "Invalid option: -${OPTARG}."
      usage
      exit 1
      ;;
  esac
done

if [ "x$file" == "x" ]
then
    echo "No file specified"
    usage
    exit 1
fi

echo "name ($name), hash ($hash), file($file), all($all)"

if [ "x$hash" != "x" ]
then
    if [ $all -eq 1 ]
    then
        get_all_dep $hash $file
    else
        get_hash_dep $hash $file
    fi
fi

if [ "x$name" != "x" ]
then
    get_name_dep $name $file
fi
