#!/usr/bin/env sh

set -o errexit
set -o pipefail
set -o nounset

cd ${CI_PROJECT_DIR}
./ci/setup_spack.sh

cd ${CI_PROJECT_DIR}
./ci/install_compilers.sh

cd  ${CI_PROJECT_DIR}
./ci/concretize.sh --force -U

cd ${CI_PROJECT_DIR}
./ci/install.sh
