#!/usr/bin/env sh

set -o pipefail
set -o nounset
set -o errexit
source ${CI_PROJECT_DIR}/ci/stack_env.sh

mkdir -p ${CI_DATA_ST}/overlayfs
mkdir -p ${CI_DATA_ST}/homes
mkdir -p ${CI_DATA_ST}/deconcretize

for i in squashfs-cache buildcache/$environment spack-mirror
do
    if [ ! -e ${CI_DATA_LT}/$i ]
    then
        mkdir -p ${CI_DATA_LT}/$i
    fi
done

cd ${CI_DATA_LT}/squashfs-cache
set +o errexit
# Check if MR as squashfs
sqfs_image=$(ls -t1 ${image_name}.sqfs 2> /dev/null | head -1)
if [ "x${sqfs_image}" == "x" ]
then
    echo "No MR ($SQUASHFS_ID-${CI_PIPELINE_ID}) squashfs found"
    echo "  - while looking for ${image_name}.sqfs"
    # look for latest release version
    sqfs_base_image=$(ls -t1 releases/${stack}-${environment}*.sqfs 2> /dev/null | head -1)

    # if no release look for main branch
    if [  "x${sqfs_base_image}" == "x" ]
    then
       sqfs_base_image=$(ls -t1 ${stack}-${environment}-${squash_base}*.sqfs 2> /dev/null | head -1)
    fi

    if [  "x${sqfs_base_image}" == "x" ]
    then
        echo "No release nor default branch ($squash_base) squashfs found"
        echo "  while looking for"
        echo "  - releases/${stack}-${environment}*.sqfs"
        echo "  - ${stack}-${environment}-${squash_base}*.sqfs"

        sqfs_base_image="${stack}-${environment}-${squash_base}-initial.sqfs"
        if [ ! -f "${sqfs_base_image}" ]
        then
            set -o errexit
            echo "Initial build creating an empty one"
            empty=$(mktemp -d)
            mkdir ${empty}/${stack}
            mksquashfs ${empty} ${sqfs_base_image}
        else
            echo "ERROR: Not initial build and no main image found"
            exit -10
        fi
    else
        echo "Found ${sqfs_base_image}"
    fi
    set -o errexit

    # Link MR sqaushfs to the one of default branch
    sqfs_image=${image_name}.sqfs

    ln -sf ${sqfs_base_image} ${sqfs_image}

    echo "Linking ${sqfs_image} -> ${sqfs_base_image}"
else
    set -o errexit
    echo "Found ${sqfs_image}"
fi
cd -
