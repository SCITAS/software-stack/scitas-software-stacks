#!/usr/bin/env sh

CI_DATA=$1
shift
suffix=$1
shift
sqfs_image=$1
shift
mount_point="$*"

if [ -d ${CI_DATA} ]
then
    for i in upper wd
    do
        mkdir -p ${CI_DATA}/overlayfs/$i-${suffix}
    done
fi

#echo "Preparing overlay fs at ${CI_DATA}/overlayfs"
echo "squashfuse_ll $sqfs_image $mount_point"

/usr/libexec/apptainer/bin/squashfuse_ll  $sqfs_image $mount_point
