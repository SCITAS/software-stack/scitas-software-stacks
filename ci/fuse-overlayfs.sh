#!/usr/bin/env sh

suffix=$1
shift
subdir=$1
shift
mount_point=$1

#echo "fuse-overlayfs"
#echo "    -o squash_to_uid=$(id -u)"
#echo "    -o squash_to_gid=$(id -g)"
#echo "    -o lowerdir=/overlayfs/lower-${suffix}"
#echo "    -o upperdir=/overlayfs/upper-${suffix}"
#echo "    -o workdir=/overlayfs/wd-${suffix}"
#echo "    $mount_point"

#/usr/libexec/apptainer/bin/fuse-overlayfs \
fuse-overlayfs \
    -o squash_to_uid=$(id -u) \
    -o squash_to_gid=$(id -g) \
    -o lowerdir=/overlayfs/lower-${suffix}/${subdir} \
    -o upperdir=/overlayfs/upper-${suffix} \
    -o workdir=/overlayfs/wd-${suffix} \
    $*
